/*
JavaScript Document

FinalVadaszy
10/27/15

Sticky menu control
*/

var $LV = jQuery.noConflict();
//$LV('head').append('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">');



$LV( document ).ready(function(){

			
		var stickyNavTop = $LV('#sticky-horizontal-menu').offset().top;
		
 
		var stickyNav = function(){
		var scrollTop = $LV(window).scrollTop();
		
		      
		if (scrollTop > stickyNavTop) { 
		    $LV('#sticky-horizontal-menu').addClass('sticky');
		} else {
		    $LV('#sticky-horizontal-menu').removeClass('sticky'); 
		}
		};
		 
		stickyNav();
		 
		$LV(window).scroll(function() {
		    stickyNav();
		});



})